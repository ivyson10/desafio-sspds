

function up() {
  echo "Subindo os microserviços"
  cd eureka-server
  ./mvnw clean install -DskipTests

  cd ../gateway-service
  echo "Construindo o gateway"
  ./mvnw clean install -DskipTests 

  cd ../managment-service
  echo "Construindo o gerenciador de usuários"
  ./mvnw clean install -DskipTests

  cd ../security-service
  echo "Construindo o serviço de segurança"
  ./mvnw clean install -DskipTests

  cd ..
  docker-compose up --force-recreate --build -d

}

function down() {
  echo "Derrubando todas as imagens"
  docker-compose down
}

function down_volumes() {
  echo "Derrubando todas as imagens e seus volumes"
  docker-compose down --volumes
}

function main() {

  if [ "$script_param" == "up" ]; then
    up
  elif [ "$script_param" == "down" ]; then
    down
  elif [ "$script_param" == "down_volumes" ]; then
    down_volumes
  else
    echo "cmd: '$script_param' not found!"
  fi
}

script_param=$1

main
