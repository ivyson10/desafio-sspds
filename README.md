# Microserviço de Gestão de usuários

Responsável por criar usuários ADMIN ou USER, buscar um usuário e também deletar  usuário.

# Ambiente de  deploy
Use qualquer servidor linux que tenha suporte para o openjdk 11, Docker e Docker Compose.

# Deploy no servidor 
1- Você deve ir no servidor baixar o projeto do gitlab e ir para branch [master] e executar os seguintes procedimentos.

Liberar o script 'imagesUp.sh' para ter permissão de execução execução
```
sudo chmod +x 'imagesUp.sh
```
Depois de liberar execute ele  com o parâmetro up. Esse comando vai construir os jars dos microserviços, depois  vai construir as imagens docker.
```
./imagesUp.sh up
```
Para derrubar as imagens dos microserviços que estão rodando no docker, executar:
```
./imagesUp.sh down
```
Para derrubas as imagens que estão rodando no docker junto com os volumes, executar:
```
./imagesUp.sh down_volumes
```


# Swagger
O swagger é uma documentação que  possui as rotas e os DTOs do sistema, disponível na rota:
```
http://localhost:8090/swagger-ui/index.html#/
```


# Rota especial
Foi criado uma rota para criar admin
```
/managment/user/admin
```

# Como usar a api
Use o postman para poder consumir a api.

- Inicialmente deve criar um usuário admin, no qual não existe autenticação para isso. Na rota: '/managment/user/admin' passando somente  os dados necessário, você pode ver isso no swagger.

- Para pegar o token você precisa fazer uma request 'POST na rota:
```
localhost:9000/oauth/token
```
Passando no 'Authorization' o 'Basic Auth' :
```
Username: sspds
Password: sspds
```
No Body você deve passa um fom-data no seguinte padrão:
```
grant_type: password
username:'<Seu user>'
password:'Senha do seu user'
```

Com isso você vai obter o access token,  ele tem duração de 60 segundos mas isso pode ser mudado, ou usar o refresh token que possui duração maior. Com isso você pode acessar as rotas privadas para admin. 

# Use a rota do gateway para direcionar para os microserviços.
```
http://localhost:9000/
```

# As portas que os microserviços usam
- eureka-server:8761
- gateway-service:9000
- security-service:8091
- managment-service:8090

# Tecnologias
- Spring
- Mongo
- Swagger
- Docker
- Docker Compose
