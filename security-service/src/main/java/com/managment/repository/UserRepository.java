package com.managment.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.managment.model.User;

@Repository
public interface UserRepository  extends MongoRepository<User, String>{

	Optional<User> findOneByIdAndNameContainsIgnoreCase(String id, String name);
	
	Optional<User> findOneByName(String name);
	
	Optional<User> findOneById(String id);
	
}
