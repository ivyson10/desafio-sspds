package com.managment.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.managment.repository.UserRepository;

@Service
public class UserService implements UserDetailsService{
	
	@Autowired
	private UserRepository repository;

	@Override
	public UserDetails loadUserByUsername(String username)  {
		var user = repository.findOneByName(username);
		if(user.isEmpty())
			throw  new UsernameNotFoundException("Usuário não encontrado no sistema");
		List<String> rolesUser = user.get().getPermissions().stream().map( p -> p.getRole().toString()).collect(Collectors.toList());
		List<GrantedAuthority> auth = AuthorityUtils.createAuthorityList(rolesUser.stream().toArray(String[]:: new));
		return new User(user.get().getName(), user.get().getPassword(), auth );
	}

	
	
}
