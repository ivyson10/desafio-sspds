package com.managment.model;

import java.time.LocalDateTime;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;



public abstract class AbstractEntity {

	@Id
	private String idInternal;
	
	@CreatedDate
	private LocalDateTime createdDate;
	
	public boolean isNew() {
	    return createdDate == null;
	}
	public String getIdInternal() {
		return idInternal;
	}
	public void setIdInternal(String idInternal) {
		this.idInternal = idInternal;
	}
	public LocalDateTime getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(LocalDateTime createdDate) {
		if(this.isNew()) 
			this.createdDate = createdDate;
	}
}
