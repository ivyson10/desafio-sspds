package com.managment.model;

import com.managment.model.enumerator.Role;

public class Permission extends AbstractEntity {
	
	private String path;
	
	private Role role;
	
	public Permission() {
	}
	
	public Permission(String path, Role role) {
		this.path = path;
		this.role = role;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
}
