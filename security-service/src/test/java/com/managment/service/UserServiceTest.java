package com.managment.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.AutoConfigureDataMongo;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.managment.model.Permission;
import com.managment.model.User;
import com.managment.model.enumerator.Role;
import com.managment.repository.UserRepository;

@TestMethodOrder(MethodOrderer.Random.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureDataMongo
public class UserServiceTest {

	@Autowired
	private UserService service;

	@MockBean
	private UserRepository repository;
	
	@Test
	void tryLoadUserByUsername() {
		var user = initUser();
		var username = "ivyson";
		when(repository.findOneByName(username)).thenReturn(Optional.of(user));
		var result = service.loadUserByUsername(username);
		assertEquals(user.getName(), result.getUsername());
	}
	
	@Test
	void tryLoadUserByUsernameNotFound() {
		var username = "ivyson";
		when(repository.findOneByName(username)).thenReturn(Optional.empty());
		Throwable exception = Assertions.catchThrowable(() -> service.loadUserByUsername(username));
		assertThat(exception).isInstanceOf(UsernameNotFoundException.class);
	}
	
	private User initUser() {
		var obj = new User();
		obj.setName("ivyson");
		obj.setPermissions(Stream.of(new Permission("*", Role.ROLE_ADMIN)).collect(Collectors.toSet()));
		obj.setCreatedDate(LocalDateTime.now());
		obj.setId("3421");
		obj.setPassword("pass");
		return obj;
	}
	
}
