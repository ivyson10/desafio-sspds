package com.gateway.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringCloudConfig {

	@Bean
    public RouteLocator gatewayRoutes(RouteLocatorBuilder routeLocatorBuilder)
    {
        return routeLocatorBuilder.routes()
                .route("managment-service", rt -> rt.path("/managment/**")
                        .uri("http://managment:8090/"))
                .route("security-service", rt -> rt.path("/**")
                        .uri("http://security:8091/"))
                .build();

    }
	
}
