package com.managment.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import io.swagger.annotations.ApiModelProperty;

public class PermissionDTO {
	
	@ApiModelProperty(required = true)
	@NotBlank(message = "{path.not.blank}")
	private String path;	
	
	@ApiModelProperty(required = true)
	@NotBlank(message = "{role.not.blank}")
	@Pattern(regexp = "^(ROLE_USER|ROLE_ADMIN)$", message = "Somente ROLE_USER e ROLE_ADMIN são permitidos")
	private String role;
	
	public PermissionDTO() {
	}
	
	public PermissionDTO( String path, String role) {
		this.path = path;
		this.role = role;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	
}
