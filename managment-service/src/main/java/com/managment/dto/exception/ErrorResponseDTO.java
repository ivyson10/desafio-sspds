package com.managment.dto.exception;

import java.util.List;

public class ErrorResponseDTO {

	private  String message;
	private String key;
    private  int code;
    private  String status;
    private  String objectName;
    private  List<ErrorObjectDTO> errors;
    
	public ErrorResponseDTO() {
	}
	public ErrorResponseDTO(String message, int code, String status, String objectName, List<ErrorObjectDTO> errors, String key) {
		this.message = message;
		this.code = code;
		this.status = status;
		this.objectName = objectName;
		this.errors = errors;
		this.key = key;
	}
	public String getKey() {
		return key;
	}
	public String getMessage() {
		return message;
	}
	public int getCode() {
		return code;
	}
	public String getStatus() {
		return status;
	}
	public String getObjectName() {
		return objectName;
	}
	public List<ErrorObjectDTO> getErrors() {
		return errors;
	}
	
}
