package com.managment.dto.exception;

public class ErrorObjectDTO {

	private  String message;
	
    private  String field;
    
    private  Object parameter;
    
    public ErrorObjectDTO() {
	}
    
	public ErrorObjectDTO(String message, String field, Object parameter) {
		this.message = message;
		this.field = field;
		this.parameter = parameter;
	}
	public String getMessage() {
		return message;
	}
	public String getField() {
		return field;
	}
	public Object getParameter() {
		return parameter;
	}
	
}
