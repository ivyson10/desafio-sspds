package com.managment.dto;

import javax.validation.constraints.NotBlank;

import io.swagger.annotations.ApiModelProperty;

public class UserAdminDTO {

	@ApiModelProperty(required = true)
	@NotBlank(message = "{id.not.blank}")
	private String id;

	@ApiModelProperty(required = true)
	@NotBlank(message = "{name.not.blank}")
	private String name;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
