package com.managment.dto;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

import io.swagger.annotations.ApiModelProperty;


public class UserDTO {

	@ApiModelProperty(required = true)
	@NotBlank(message = "{id.not.blank}")
	private String id;

	@ApiModelProperty(required = true)
	@NotBlank(message = "{name.not.blank}")
	private String name;

	@ApiModelProperty(required = true)
	@NotEmpty(message = "{permissions.not.empty}")
	@Valid
	private List<PermissionDTO> permissions;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<PermissionDTO> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<PermissionDTO> permissions) {
		this.permissions = permissions;
	}

}
