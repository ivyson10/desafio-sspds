package com.managment.config;

import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;

@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
@EnableWebSecurity
@Configuration
public class ResourceServerConfig extends WebSecurityConfigurerAdapter {

	private static final String[] AUTH_WHITELIST = { "/v2/api-docs", "/v3/api-docs", "/swagger-resources/**",
			"/swagger-ui/**" };

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable().authorizeRequests().antMatchers(AUTH_WHITELIST).permitAll()
				.antMatchers( "/managment/user/admin/**").permitAll()
				.antMatchers(HttpMethod.GET, "/managment/user/**").permitAll()
				.anyRequest().authenticated().and()
				.oauth2ResourceServer()
				.jwt().jwtAuthenticationConverter(grantedAuthoritiesExtractor());
	}

	private final Converter<Jwt, AbstractAuthenticationToken> grantedAuthoritiesExtractor() {
		var jwtConverter = new JwtAuthenticationConverter();
		jwtConverter.setJwtGrantedAuthoritiesConverter(GrantedAuthoritiesExtractor.newExtractor());
		return jwtConverter;
	}

	private final static class GrantedAuthoritiesExtractor implements Converter<Jwt, Collection<GrantedAuthority>> {

		public static GrantedAuthoritiesExtractor newExtractor() {
			return new GrantedAuthoritiesExtractor();
		}

		public Collection<GrantedAuthority> convert(Jwt jwt) {
			return jwt.getClaimAsStringList("authorities").stream().map(SimpleGrantedAuthority::new)
					.collect(Collectors.toList());
		}
	}

}
