package com.managment.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebMvc
public class CorsConfig implements WebMvcConfigurer {

	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**")
				.allowedMethods("*").allowedOrigins("*")
				.allowedHeaders("*")
				.exposedHeaders("Access-Control-Allow-Origin");
	}
//	public void addCorsMappings(CorsRegistry registry) {
//		registry.addMapping("/**").allowCredentials(true).allowedMethods("GET", "POST", "PUT", "DELETE", "OPTIONS", "HEAD").allowedOrigins("*")
//				.allowedHeaders("X-Requested-With", "Origin", "Content-Type", "Accept", "Authorization").exposedHeaders( "Access-Control-Allow-Origin");
//	}

//	@Bean
//	public FilterRegistrationBean<CorsFilter> corsFilter() {
//		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
//
//		CorsConfiguration configAutenticacao = new CorsConfiguration();
//		configAutenticacao.setAllowCredentials(true);
//		configAutenticacao.setAllowedOriginPatterns(Collections.singletonList("*"));
//		configAutenticacao.addAllowedHeader("Content-Type");
//		configAutenticacao.addAllowedHeader("Authorization");
//		configAutenticacao.addAllowedHeader("Accept");
//		configAutenticacao.addAllowedMethod("POST");
//		configAutenticacao.addAllowedMethod("GET");
//		configAutenticacao.addAllowedMethod("DELETE");
//		configAutenticacao.addAllowedMethod("HEAD");
//		configAutenticacao.addAllowedMethod(HttpMethod.OPTIONS.name());
//		configAutenticacao.setMaxAge(3600L);
//		source.registerCorsConfiguration("/**", configAutenticacao); // Global for all paths
//
//		FilterRegistrationBean<CorsFilter> bean = new FilterRegistrationBean<CorsFilter>(new CorsFilter(source));
//		bean.setOrder(Ordered.HIGHEST_PRECEDENCE);
//		return bean;
//	}

//	@Bean
//	public FilterRegistrationBean<CorsFilter> corsFilterRegistrationBean() {
//	    CorsConfiguration config = new CorsConfiguration();
//	    config.setAllowCredentials(true);
//	    config.setAllowedOrigins(Collections.singletonList("*"));
//	    config.setAllowedMethods(Collections.singletonList("*"));
//	    config.setAllowedHeaders(Collections.singletonList("*"));
//
//	    UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
//	    source.registerCorsConfiguration("/**", config);
//
//	    FilterRegistrationBean<CorsFilter> bean = new FilterRegistrationBean<>();
//	    bean.setFilter(new CorsFilter(source));
//	    bean.setOrder(Ordered.HIGHEST_PRECEDENCE);
//
//	    return bean;
//	}

}
