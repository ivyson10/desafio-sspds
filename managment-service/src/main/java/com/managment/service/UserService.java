package com.managment.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.managment.dto.GetUserDTO;
import com.managment.dto.MessageDTO;
import com.managment.dto.PermissionDTO;
import com.managment.dto.UserAdminDTO;
import com.managment.dto.UserDTO;
import com.managment.exception.ConflictException;
import com.managment.exception.NotFoundException;
import com.managment.model.Permission;
import com.managment.model.User;
import com.managment.model.enumerator.Role;
import com.managment.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository repository;
	
	
	public MessageDTO saveUser(UserDTO dto) {
		verifyUserExisting(dto.getId(), dto.getName());
		var newUser = userDTOToUser(dto);
		repository.save(newUser);
		return new MessageDTO("O usuário foi criado com sucesso", "user.created.success");
	}
	
	public MessageDTO saveUserAdmin(UserAdminDTO dto) {
		verifyUserExisting(dto.getId(), dto.getName());
		var newUser = userAdminDTOToUser(dto);
		repository.save(newUser);
		return new MessageDTO("O usuário foi criado com sucesso", "user.created.success");
	}
	
	private User userDTOToUser(UserDTO dto) {
		var obj = new User();
		obj.setName(dto.getName());
		obj.setPermissions(dto.getPermissions().stream().map(p -> new Permission(p.getPath(), Role.valueOf(p.getRole()))).collect(Collectors.toSet()));
		obj.setCreatedDate(LocalDateTime.now());
		obj.setId(dto.getId());
		obj.setPassword(new BCryptPasswordEncoder().encode(dto.getName()));
		return obj;
	}
	
	private User userAdminDTOToUser(UserAdminDTO dto) {
		var obj = new User();
		obj.setName(dto.getName());
		obj.setPermissions(Stream.of(new Permission("*", Role.ROLE_ADMIN)).collect(Collectors.toSet()));
		obj.setCreatedDate(LocalDateTime.now());
		obj.setId(dto.getId());
		obj.setPassword(new BCryptPasswordEncoder().encode(dto.getName()));
		return obj;
	}
	
	public GetUserDTO getUser(String id) {
		var user = findOneById(id);
		return userToGetUserDTO(user);
	}
	
	private GetUserDTO userToGetUserDTO(User obj) {
		var dto = new GetUserDTO();
		dto.setId(obj.getId());
		dto.setName(obj.getName());
		dto.setDateCreate(obj.getCreatedDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
		dto.setPermissions(obj.getPermissions().stream().map(p -> new PermissionDTO(p.getPath(), p.getRole().toString())).collect(Collectors.toList()));
		return dto;
	}
	
	private User findOneById(String id) {
		return repository.findOneById(id).orElseThrow(() -> new NotFoundException("Não existe usuário com esse id: " + id,"notfound.user.id" ));
	}
	
	public MessageDTO deleteUser(String id, String responsible ) {
		var user = findOneById(id);
		if(responsible.equals(user.getName()))
			throw new ConflictException("Não é possível deletar seu próprio usuário", "user.delete.conflict");
		repository.delete(user);
		return new MessageDTO("Usuário deletado com sucesso", "user.deleted.success");
	}
	
	private void verifyUserExisting(String id, String name) {
		var user = repository.findOneByIdAndName(id, name);
		if(user.isPresent())
			throw new ConflictException("Já existe um usuário com esse Id: " + id + " e com esse nome: " +  name, "existing.user");
		
		 user = repository.findOneById(id);
		if(user.isPresent())
			throw new ConflictException("Já existe um usuário com esse Id: " + id , "existing.user");
		
		 user = repository.findOneByName(name);
		if(user.isPresent())
			throw new ConflictException("Já existe um usuário com esse nome: " +  name, "existing.user");
		
	}
	
}
