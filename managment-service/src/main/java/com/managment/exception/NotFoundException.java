package com.managment.exception;

import org.springframework.http.HttpStatus;

public class NotFoundException extends ManagmentException {

	private static final long serialVersionUID = 1L;
	
	public NotFoundException( String message, String key) {
		super(HttpStatus.NOT_FOUND, message, key);
	}

}
