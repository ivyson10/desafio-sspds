package com.managment.exception;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.managment.dto.MessageDTO;
import com.managment.dto.exception.ErrorObjectDTO;
import com.managment.dto.exception.ErrorResponseDTO;

@RestControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

	private static final Logger logger = LoggerFactory.getLogger(RestExceptionHandler.class);
	
	@ExceptionHandler(ManagmentException.class)
	protected ResponseEntity<MessageDTO> handleManagmentException(ManagmentException exception) {
		logger.error("[environment-dev]", exception);
		return exception.getMessageError();
	}
	
	@ExceptionHandler(AccessDeniedException.class)
	protected ResponseEntity<MessageDTO> handleAccessDeniedException(AccessDeniedException exception) {
		logger.error("[environment-dev]", exception);
		return ResponseEntity.status(HttpStatus.FORBIDDEN)
				.body(new MessageDTO(exception.getMessage(),
						"access.forbidden"));
	}
	
	@ExceptionHandler(Exception.class)
	protected ResponseEntity<MessageDTO> handleException(Exception ex) {
		logger.error("[environment-dev] Exception não tratada: ", ex);
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
				.body(new MessageDTO("Exception não tratada: " + ex.toString() + ", veja os logs do microserviço",
						"exception.handler"));
	}
	
	 @Override
	    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
	        List<ErrorObjectDTO> errors = getErrors(ex);
	        ErrorResponseDTO errorResponse = getErrorResponse(ex, status, errors);
	        return new ResponseEntity<>(errorResponse, status);
	    }

	    private ErrorResponseDTO getErrorResponse(MethodArgumentNotValidException ex, HttpStatus status, List<ErrorObjectDTO> errors) {
	        return new ErrorResponseDTO("Requisição possui campos inválidos", status.value(),
	                status.getReasonPhrase(), ex.getBindingResult().getObjectName(), errors,"request.invalid.fields");
	    }
	private List<ErrorObjectDTO> getErrors(MethodArgumentNotValidException ex) {
	    return ex.getBindingResult().getFieldErrors().stream()
	            .map(error -> new ErrorObjectDTO(error.getDefaultMessage(), error.getField(), error.getRejectedValue()))
	            .collect(Collectors.toList());
	}
	
}
