package com.managment.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.managment.dto.MessageDTO;

public class ManagmentException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private final HttpStatus status;

	private final String key;
	
	public ManagmentException(HttpStatus status, String message, String key) {
		super(message);
		this.status = status;
		this.key = key;
	}
	
	public ResponseEntity<MessageDTO> getMessageError() {
		return ResponseEntity.status(status).body(new MessageDTO(getMessage(), key));
	}

	public HttpStatus getStatus() {
		return status;
	}

	public String getKey() {
		return key;
	}
	
}
