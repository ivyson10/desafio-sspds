package com.managment.exception;

import org.springframework.http.HttpStatus;

public class ConflictException extends  ManagmentException {

	private static final long serialVersionUID = 1L;
	
	public ConflictException( String message, String key) {
		super(HttpStatus.CONFLICT, message, key);
	}
}
