package com.managment.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.security.oauth2.jwt.Jwt;

import com.managment.dto.GetUserDTO;
import com.managment.dto.MessageDTO;
import com.managment.dto.UserAdminDTO;
import com.managment.dto.UserDTO;
import com.managment.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value = "Crud do gerenciamento de usuários")
@RequestMapping("/managment/user")
public class UserController {

	@Autowired
	private UserService service;
	
	@ApiOperation(value = "O usuário Admin pode criar novos usuários")
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "O usuário foi criado com sucesso | key: user.created.success"),
			@ApiResponse(code = 409, message = "Um usuário já existe | key: existing.user"), })
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping
	@ResponseStatus(value = HttpStatus.CREATED)
	public ResponseEntity<MessageDTO> saveUser(@Valid  @RequestBody  UserDTO dto){
		return ResponseEntity.status(HttpStatus.CREATED).body(service.saveUser(dto));
	}
	
	
	@ApiOperation(value = "Qualquer usuário pode fazer uma busca por usuário")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "O usuário foi retornado com sucesso"),
			@ApiResponse(code = 404, message = "Não existe usuário com esse id | key: notfound.user.id"), })
	@GetMapping("/{id}")
	public ResponseEntity<GetUserDTO> getUser( @PathVariable String id){
		return ResponseEntity.ok(service.getUser(id));
	}
	
	@ApiOperation(value = "Somente usuário com permissão de Amin pode deletar um usuário")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Usuário deletado com sucesso  | key: user.deleted.success "),
			@ApiResponse(code = 404, message = "Não existe usuário com esse id | key: notfound.user.id"), })
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@DeleteMapping("/{id}")
	public ResponseEntity<MessageDTO> deleteUser(@AuthenticationPrincipal Jwt jwt, @PathVariable String id){
		return ResponseEntity.ok(service.deleteUser(id, jwt.getClaimAsString("user_name")));
	}
	
	@ApiOperation(value = "Cria o usuário Admin")
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "O usuário foi criado com sucesso | key: user.created.success"),
			@ApiResponse(code = 409, message = "Um usuário já existe | key: existing.user"), })
	@PostMapping("admin")
	@ResponseStatus(value = HttpStatus.CREATED)
	public ResponseEntity<MessageDTO> saveUserAdmin(@Valid  @RequestBody  UserAdminDTO dto){
		return ResponseEntity.status(HttpStatus.CREATED).body(service.saveUserAdmin(dto));
	}
	
}
	
