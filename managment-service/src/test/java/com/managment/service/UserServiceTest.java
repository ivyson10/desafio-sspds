package com.managment.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.AutoConfigureDataMongo;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.managment.dto.PermissionDTO;
import com.managment.dto.UserAdminDTO;
import com.managment.dto.UserDTO;
import com.managment.exception.ConflictException;
import com.managment.exception.NotFoundException;
import com.managment.model.Permission;
import com.managment.model.User;
import com.managment.model.enumerator.Role;
import com.managment.repository.UserRepository;

@TestMethodOrder(MethodOrderer.Random.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureDataMongo
public class UserServiceTest {

	@Autowired
	private UserService service;

	@MockBean
	private UserRepository repository;

	@Test
	void trySaveUser() {
		var userDTO = initUserDTO();
		var user = userDTOToUser(userDTO);
		when(repository.save(user)).thenReturn(user);
		var result = service.saveUser(userDTO);
		assertEquals("user.created.success", result.getKey());
	}

	@Test
	void trySaveUserExistingIdAndName() {
		var userDTO = initUserDTO();
		var user = userDTOToUser(userDTO);
		when(repository.findOneByIdAndName(userDTO.getId(), userDTO.getName())).thenReturn(Optional.of(user));
		Throwable exception = Assertions.catchThrowable(() -> service.saveUser(userDTO));
		assertThat(exception).isInstanceOf(ConflictException.class);
	}

	@Test
	void trySaveUserExistingId() {
		var userDTO = initUserDTO();
		var user = userDTOToUser(userDTO);

		when(repository.findOneById(userDTO.getId())).thenReturn(Optional.of(user));
		Throwable exception = Assertions.catchThrowable(() -> service.saveUser(userDTO));
		assertThat(exception).isInstanceOf(ConflictException.class);

	}
	
	@Test
	void trySaveUserExistingName() {
		var userDTO = initUserDTO();
		var user = userDTOToUser(userDTO);

		when(repository.findOneByName(userDTO.getName())).thenReturn(Optional.of(user));
		Throwable exception = Assertions.catchThrowable(() -> service.saveUser(userDTO));
		assertThat(exception).isInstanceOf(ConflictException.class);

	}

	@Test
	void trySaveUserAdmin() {
		var userDTO = initUserAdminDTO();
		var user = userAdminDTOToUser(userDTO);
		when(repository.save(user)).thenReturn(user);
		var result = service.saveUserAdmin(userDTO);
		assertEquals("user.created.success", result.getKey());
	}

	@Test
	void tryGetUser() {
		var id = "3421";
		var userDTO = initUserDTO();
		var user = userDTOToUser(userDTO);
		when(repository.findOneById(id)).thenReturn(Optional.of(user));
		var result = service.getUser(id);
		assertEquals(userDTO.getId(), result.getId());
	}

	@Test
	void tryGetUserNotFound() {
		var id = "3421";
		when(repository.findOneById(id)).thenReturn(Optional.empty());
		Throwable exception = Assertions.catchThrowable(() -> service.getUser(id));
		assertThat(exception).isInstanceOf(NotFoundException.class);
	}

	@Test
	void tryDeleteUser() {
		var id = "3421";
		var responsible = "resp";
		var userDTO = initUserDTO();
		var user = userDTOToUser(userDTO);
		when(repository.findOneById(id)).thenReturn(Optional.of(user));
		doNothing().when(repository).delete(user);
		var result = service.deleteUser(id, responsible);
		assertEquals("user.deleted.success", result.getKey());
	}
	
	@Test
	void tryDeleteUserHimself() {
		var id = "3421";
		var userDTO = initUserDTO();
		var user = userDTOToUser(userDTO);
		var responsible = user.getName();
		when(repository.findOneById(id)).thenReturn(Optional.of(user));
		doNothing().when(repository).delete(user);
		Throwable exception = Assertions.catchThrowable(() -> service.deleteUser(id, responsible));
		assertThat(exception).isInstanceOf(ConflictException.class);
	}

	private UserDTO initUserDTO() {
		var dto = new UserDTO();
		dto.setId("3421");
		dto.setName("ivyson");
		dto.setPermissions(Arrays.asList(new PermissionDTO("*", "ROLE_ADMIN")));
		return dto;
	}

	private UserAdminDTO initUserAdminDTO() {
		var dto = new UserAdminDTO();
		dto.setId("3421");
		dto.setName("ivyson");
		return dto;
	}

	private User userDTOToUser(UserDTO dto) {
		var obj = new User();
		obj.setName(dto.getName());
		obj.setPermissions(dto.getPermissions().stream()
				.map(p -> new Permission(p.getPath(), Role.valueOf(p.getRole()))).collect(Collectors.toSet()));
		obj.setCreatedDate(LocalDateTime.now());
		obj.setId(dto.getId());
		return obj;
	}

	private User userAdminDTOToUser(UserAdminDTO dto) {
		var obj = new User();
		obj.setName(dto.getName());
		obj.setPermissions(Stream.of(new Permission("*", Role.ROLE_ADMIN)).collect(Collectors.toSet()));
		obj.setCreatedDate(LocalDateTime.now());
		obj.setId(dto.getId());
		obj.setPassword(new BCryptPasswordEncoder().encode(dto.getName()));
		return obj;
	}

}
